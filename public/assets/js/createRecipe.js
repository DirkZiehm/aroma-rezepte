
let index = 1;
$('.theme-btn-add').click(function () {
    index++;
    $('.ingredients-add').append(getNewIngredientsFields(index));
    bindDelete();
});


function getNewIngredientsFields(index) {
    return '<div class="copy_'+index+'"><select name="ingredients['+index+'][oil]" >\n' +
        '<option value="Pflegeöl" selected>Pflegeöl</option>\n' +
        '<option value="Wasser" >Wasser</option>\n' +
        '<option value="Zitrone" >Zitrone</option>\n' +
        '<option value="Orange" >Orange</option>\n' +
        '<option value="Bergamotte" >Bergamotte</option>\n' +
        '<option value="Eukalyptus" >Eukalyptus</option>\n' +
        '<option value="Pfefferminz" >Pfefferminz</option>\n' +
        '<option value="Lavendel" >Lavendel</option>\n' +
        '<option value="Thymian" >Thymian</option>\n' +
        '<option value="Thymian" >Zedern</option>\n' +
        '</select>\n' +
        '<input class="create-lot-input" type="number" name="ingredients['+index+'][lot]" required="required" value="100">\n' +
        '<select name="ingredients['+index+'][unit]" >\n' +
        '<option value="ML" selected>ML</option>\n' +
        '<option value="Tropfen">Tropfen</option>\n' +
        '</select>\n' +
        '<span data-delete="'+index+'" class="delete theme-btn-danger"><i class="far fa-trash-alt"></i></span>\n' +
        '</div>';
}

function bindDelete() {
    $(".delete").unbind('click');

    $('.delete').click(function(){
        let index =  $(this).data('delete');
        console.log(index);
        $('.copy_' + index).remove();
    });
}


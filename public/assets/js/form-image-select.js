$('.select-pre-image').each(function () {
    $(this).click(function () {
        let val = $(this).attr('src');
        $('.preview-image-text').first().val(val);
        $('.preview-image').first().attr('src',val);
        $('.close').click();
    });
});

$('.select-pre-image-banner').each(function () {
    $(this).click(function () {
        let val = $(this).attr('src');
        $('.preview-image-banner-text').first().val(val);
        $('.preview-image-banner').first().attr('src',val);
        $('.close').click();
    });
});

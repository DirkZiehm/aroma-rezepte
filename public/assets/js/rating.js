console.log('rating');

let choose = false;
let stars = 0;

$('.rating-star').hover(function() {
    let count = $( this ).data('rating');

    $('.rating-star').each(function () {
        $( this ).removeClass( 'star-full' );
        $( this ).addClass( 'star-empty' );

    });

    setStars(count);
});

$('.rating-star').click(function () {
    stars = $( this ).data('rating');
    choose = true;
    $('.rating-stars-input').val(stars);
});

$( ".rating-star" ).mouseout(function() {
    setStars(0);
    if (choose === true) {
        setStars(stars);
        return;
    }

    $('.rating-star').each(function () {
        $( this ).removeClass( 'star-full' );
        $( this ).addClass( 'star-empty' );
    });

});

function setStars(count) {
    $('.rating-star').each(function () {
        $( this ).addClass( 'star-empty' );
        $( this ).removeClass( 'star-full' );
    });

    let i = 0;
    $('.rating-star').each(function () {
        if (i >= count) {
            return;
        }
        $( this ).removeClass( 'star-empty' );
        $( this ).addClass( 'star-full' );
        i++;
    });
}
<?php

use App\Kernel;

require_once dirname(__DIR__).'/vendor/autoload_runtime.php';

// workaround to get the referrer
$_SESSION['ref'] = $_SERVER['HTTP_REFERER'];
return function (array $context) {
    return new Kernel($context['APP_ENV'], (bool) $context['APP_DEBUG']);
};



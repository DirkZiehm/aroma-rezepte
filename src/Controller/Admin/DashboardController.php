<?php

namespace App\Controller\Admin;

use App\Entity\BlogArticle;
use App\Entity\Favorite;
use App\Entity\Recipe;
use App\Entity\RecipeComment;
use App\Entity\Tracker;
use App\Entity\User;
use App\Entity\UserSetup;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        $routeBuilder = $this->get(AdminUrlGenerator::class);

        return $this->redirect($routeBuilder->setController(ListeCrudController::class)->generateUrl());

    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Aroma Rezepte');
    }

    public function configureMenuItems(): iterable
    {
        return [
            MenuItem::linkToDashboard('Dashboard'),
            MenuItem::section('Blog'),
            MenuItem::linkToCrud('Blog Article','fa fa-file-text',BlogArticle::class),
            MenuItem::section('Rezepte'),
            MenuItem::linkToCrud('Rezept','fa fa-comment',Recipe::class),
            MenuItem::linkToCrud('Rezept Kommentare','fa fa-comment',RecipeComment::class),
            MenuItem::section('User'),
            MenuItem::linkToCrud('Benutzer','fa fa-user',User::class),
            MenuItem::linkToCrud('Setup','fa fa-user',UserSetup::class),
            MenuItem::linkToCrud('Favoriten','fa fa-user',Favorite::class),
            MenuItem::section('Tracker'),
            MenuItem::linkToCrud('Tracker','',Tracker::class),
//            MenuItem::subMenu('User', 'fa fa-article')->setSubItems([
//                MenuItem::linkToCrud('Benutzer', 'fa fa-tags', User::class),
//                MenuItem::linkToCrud('Setup', 'fa fa-file-text', UserSetup::class),
//                MenuItem::linkToCrud('Favoriten', 'fa fa-comment', Favorite::class),
//            ]),
        ];
    }
}

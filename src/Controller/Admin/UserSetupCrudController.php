<?php

namespace App\Controller\Admin;

use App\Entity\UserSetup;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class UserSetupCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return UserSetup::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}

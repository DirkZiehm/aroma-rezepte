<?php

namespace App\Controller;

use App\Entity\BlogArticle;
use App\Repository\BlogArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BlogController extends AbstractController
{
    #[Route('/blog', name: 'blog')]
    public function index(Request $request): Response
    {
        $userID =  ($this->getUser() !== null) ? $this->getUser()->getId() : 0;
        $tracker = new TrackerController();
        $tracker->track($request,$this->getDoctrine()->getManager(),$userID);

        $articles = $this->
                    getDoctrine()->
                    getRepository(BlogArticle::class)->
                    getOrderedArticles();


        return $this->render('blog/index.html.twig', [
            'controller_name' => 'BlogController',
            'articles' => $articles
        ]);
    }

    private function insertNewBolArticle()
    {
        $entityManager = $this->getDoctrine()->getManager();

        // creates a task object and initializes some data for this example
        $task = new BlogArticle();
        $task->setTitle('LR Soul of Nature PFLEGEÖL');
        $task->setImage('/assets/images/logo.png');
        $task->setDescription('');
        $task->setAuthor('aroma-rezepte.de');
        $task->setPublish('27 September 2021');
        $task->setContent('
        <div class="article-detail">
    <div class="container">
        <div class="article-img">
            <img class="img-fluid" src="/assets/images/resources/blog-detail.jpg" alt="">
        </div>
        <div class="row">
            <div class="col-md-10 mx-auto">
                <div class="article-content">
                    <div class="article-top-content">

                        <h2 class="my-3">LR Soul of Nature PFLEGEÖL</h2>
                        <div class="article-detail-meta d-flex align-items-center mb-4">
                            <span class="fs-16">
                                <img width="41" class="rounded-circle mr-2" src="/assets/images/logo.png" alt="">
                                By <a href="" class="font-weight-bold">aroma-rezepte.de</a>
                            </span>
                        </div>
                    </div>
                    <p class="font-weight-bold">ZIELGRUPPE</p>
                    <p>ür alle, die ein duftneutrales, reichhaltiges Körper-/Massageöl bevorzugen oder ein Körper-/Massageöl das
                        sich nach den individuellen Vorlieben durch die Beigabe von naturreinen ätherischen Öle beduften lässt.</p>

                    <p class="font-weight-bold">EIGENSCHAFTEN</p>
                    <p>ildes, duftneutrales Körperpflege und Massageöl für ein samtweiches,
                        geschmeidiges Hautgefühl. Nährt und regeneriert die Haut mit Mandel- und Jojobaöl und Aloe Vera Öl Extrakt,
                        spendet Feuchtigkeit und schützt die Haut vor dem austrocknen.
                        Perfekt geeignet als Basis-Öl, das mit den ätherischen Einzelölen von LR
                        Soul of Nature beduftet werden kann. Zur äußerlichen Anwendung auf der Haut geeignet.
                    </p>

                    <p class="font-weight-bold">ART DER ANWENDUNG</p>
                    <p>
                        Als Körperpflege-Öl, z.B. nach dem
                        Duschen, sanft in die Haut einmassieren. Als Massageöl etwas großzügiger auftragen und mit sanften
                        Bewegungen oder auch sanftem
                        Druck zur Massage verwenden.
                        Um das Wohlbefinden individuell zu
                        unterstützen können die LR Soul
                        of Nature Einzel-Öle in das LR Soul
                        of Nature Basis Pflegeöl gemischt
                        werden. Für diese Beduftung des
                        Körperöls kann man sich von unseren
                        DIY Rezeptideen inspirieren lassen.
                        Bitte immer an die im jeweiligen
                        Rezept angegebenen Mengen halten.
                        Vorgehensweise: Die angegebene
                        Menge der ätherischen Öle direkt in
                        die Flasche des Pflegeöls geben, da nach die Flasche wieder gut verschließen und schütteln. Fertig ist das DIY
                        Massageöl.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
        ');


        $entityManager->persist($task);
        $entityManager->flush();
    }
}

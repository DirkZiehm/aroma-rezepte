<?php

namespace App\Controller;

use App\Entity\BlogArticle;
use App\Repository\FavoriteRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BlogDetailController extends AbstractController
{
    #[Route('/blog/detail', name: 'blog_detail')]
    public function index(Request $request): Response
    {
        $userID =  ($this->getUser() !== null) ? $this->getUser()->getId() : 0;
        $tracker = new TrackerController();
        $tracker->track($request,$this->getDoctrine()->getManager(),$userID);

        $blogArticleId = $request->query->get('id');

        if ($blogArticleId === null) {
            return $this->redirectToRoute('index');
        }

        $blogArticle = $this->getDoctrine()->getRepository(BlogArticle::class)->find($blogArticleId);

        return $this->render('blog_detail/index.html.twig', [
            'article' => $blogArticle
        ]);
    }
}

<?php

namespace App\Controller;

use App\Repository\BlogArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BlogEditController extends AbstractController
{
    #[Route('/blog/edit', name: 'blog_edit')]
    public function index(Request $request,BlogArticleRepository $blogArticleRepository): Response
    {
        $blogArticle = $blogArticleRepository->find($request->get('id'));

        if ($request->getRealMethod() === 'POST') {

            $blogArticle->setTitle($request->get('title'));
            $blogArticle->setDescription($request->get('description'));
            $blogArticle->setImage($request->get('image'));
            $blogArticle->setContent($request->get('content'));
            $blogArticle->setBanner($request->get('banner'));
            $blogArticle->setActive(($request->get('active') != null));

            $em = $this->getDoctrine()->getManager();
            $em->persist($blogArticle);
            $em->flush();

            $blogArticles = $blogArticleRepository->findBy(['userID' => $this->getUser()->getId()],['id' => 'DESC']);
            return $this->render('my_blog/index.html.twig', [
                'blogArticles' => $blogArticles
            ]);
        }

        return $this->render('blog_edit/index.html.twig', [
            'blogArticle' => $blogArticle
        ]);
    }

}

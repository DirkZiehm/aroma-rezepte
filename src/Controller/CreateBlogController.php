<?php

namespace App\Controller;

use App\Entity\BlogArticle;
use App\Repository\BlogArticleRepository;
use App\Repository\UserSetupRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CreateBlogController extends AbstractController
{
    #[Route('/create/blog', name: 'create_blog')]
    public function index(Request $request, UserSetupRepository $userSetupRepository): Response
    {
        if ($request->getMethod() === 'POST') {

            $blogArticle = new BlogArticle();
            $blogArticle->setTitle($request->get('title'));
            $blogArticle->setDescription($request->get('description'));
            $blogArticle->setImage($request->get('image'));
            $blogArticle->setPublish(date('d.m.Y'));
            $blogArticle->setContent($request->get('content'));
            $blogArticle->setAuthor($this->getUser()->getUsername());
            $blogArticle->setBanner($request->get('banner'));
            $blogArticle->setUserID($this->getUser()->getId());
            $blogArticle->setActive(($request->get('active') != null));

            $userSetup = $userSetupRepository->findBy(['userID'=>$this->getUser()->getId()])[0];
            $blogArticle->setAuthorImage($userSetup->getImage());

            $em = $this->getDoctrine()->getManager();
            $em->persist($blogArticle);

            try {
                $em->flush();

            } catch (\Exception $e) {
                return $this->redirectToRoute('index');
            }

            return $this->redirectToRoute('user_setup');
        }

        return $this->render('create_blog/index.html.twig', []);
    }
}

<?php

namespace App\Controller;

use App\Entity\Recipe;
use App\Repository\RecipeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CreateRecipeController extends AbstractController
{
    #[Route('/create/recipe', name: 'create_recipe')]
    public function index(Request $request, RecipeRepository $recipeRepository): Response
    {
        if ($request->getMethod() === 'POST') {
            $recipe = new Recipe();
            $recipe->setRatings(0);
            $recipe->setRanking(0);
            $recipe->setUsername($this->getUser()->getUsername());
            $recipe->setImage('/assets/images/resources/base_oil_naked.png');
            $recipe->setDescription($request->get('description'));
            $recipe->setAdditional($request->get('additional'));
            $recipe->setTitle($request->get('title'));
            $recipe->setUserId($this->getUser()->getId());
            $recipe->setPublished(date('d.m.Y'));


            $ingredients = [];
            foreach ($request->get('ingredients') as $ingredient) {
                $overlay = $this->getOverlay($ingredient['oil']);
                $ingredients[] = [
                  'oil' => $ingredient['oil'],
                  'unit' => $ingredient['unit'],
                  'lot' => $ingredient['lot'],
                  'image' => $overlay['image'],
                  'overlay' => $overlay['overlay'],
                ];
            }

            $recipe->setIngredients($ingredients);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($recipe);
            $entityManager->flush();

            return $this->redirectToRoute('recipe');
        }

        return $this->render('create_recipe/index.html.twig', []);
    }

    private function getOverlay(string $oil)
    {
        $prefix = '/assets/images/resources/';
        $arr = [
            'Pflegeöl' => ['overlay'=> 'careoil','image' => $prefix.'care_oil.jpg'],
            'Wasser' => ['overlay'=> 'water','image' => $prefix.'water.jpg'],
            'Zitrone' => ['overlay'=> 'lemon','image' => $prefix.'lemon_oil_2.jpg'],
            'Orange' => ['overlay'=> 'orange','image' => $prefix.'orange_oil_2.jpg'],
            'Bergamotte' => ['overlay'=> 'bergamont','image' => $prefix.'bergamont_oil_2.jpg'],
            'Eukalyptus' => ['overlay'=> 'eucalyptus','image' => $prefix.'eucalyptus_oil_2.jpg'],
            'Pfefferminz' => ['overlay'=> 'peppermint','image' => $prefix.'peppermint_oil_2.jpg'],
            'Lavendel' => ['overlay'=> 'lavender','image' => $prefix.'lavender_oil_2.jpg'],
            'Thymian' => ['overlay'=> 'thyme','image' => $prefix.'thyme_oil_2.jpg'],
            'Zedern' => ['overlay'=> 'cedarwood','image' => $prefix.'cedarwood_oil_2.jpg']
            ];

        return $arr[$oil];

    }
}

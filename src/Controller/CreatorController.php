<?php

namespace App\Controller;

use App\Repository\BlogArticleRepository;
use App\Repository\RecipeRepository;
use App\Repository\UserRepository;
use App\Repository\UserSetupRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CreatorController extends AbstractController
{

    #[Route('/creator', name: 'creator')]
    public function index(Request $request,
                        UserRepository $userRepository,
                        UserSetupRepository $userSetupRepository,
                        RecipeRepository $recipeRepository,
                        BlogArticleRepository $blogArticleRepository): Response
    {

        $user = $userRepository->findUserById($request->query->get('id'))[0];

        $user->setUserSetup(
            $userSetupRepository->findBy(['userID' => $user->getId()])[0]
        );

        $recipes = $recipeRepository->findBy(['user_id' => $user->getId()]);

        $user->setRecipesCount(
            count($recipes)
        );

        $blogArticles = $blogArticleRepository->getOrderedArticles(5);

        return $this->render('creator/index.html.twig', [
            'creator' => $user,
            'recipes' => $recipes,
            'blogWidgetItems' => $blogArticles,
        ]);
    }

    #[Route('/creatorInfo', name: 'creator_info')]
    public function creatorInfo(): Response
    {
        return $this->render('creator/info.html.twig');
    }
}

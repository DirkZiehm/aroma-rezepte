<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DataPrivacyController extends AbstractController
{
    #[Route('/data/privacy', name: 'data_privacy')]
    public function index(): Response
    {
        return $this->render('data_privacy/index.html.twig', [
            'controller_name' => 'DataPrivacyController',
        ]);
    }
}

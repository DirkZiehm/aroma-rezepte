<?php

namespace App\Controller;

use App\Entity\Favorite;
use App\Repository\FavoriteRepository;
use App\Repository\RecipeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FavoritesController extends AbstractController
{
    #[Route('/favorites', name: 'favorites')]
    public function index(FavoriteRepository $favoriteRepository): Response
    {
        $favorites = $favoriteRepository->findUserFavorites($this->getUser()->getId());

        return $this->render('favorites/index.html.twig', [
            'favorites' => $favorites,
        ]);
    }

    #[Route('/favorites/add', name: 'addToFavorite')]
    public function addToFavorite(Request $request,RecipeRepository $recipeRepository): RedirectResponse
    {
        $favorite = new Favorite();
        $favorite->setUserId($this->getUser()->getId());
        $favorite->setRecipeId($request->get('id'));

        $em = $this->getDoctrine()->getManager();

        if ($recipeRepository->find($request->get('id')) === null) {
            return $this->redirectToRoute('recipe');
        }

        $em->persist($favorite);
        $em->flush();

        return $this->redirectToRoute('recipe_detail',['id' => $request->get('id')]);
    }

    #[Route('/favorites/delete', name: 'deleteToFavorite')]
    public function deleteToFavorite(Request $request, FavoriteRepository $favoriteRepository): RedirectResponse
    {
        $favorite = $favoriteRepository->findUserFavorite($this->getUser()->getId(),$request->get('id'));

        $em = $this->getDoctrine()->getManager();

        if (!isset($favorite[0])) {
            return $this->redirectToRoute('recipe');
        }

        $em->remove($favorite[0]);
        $em->flush();

        return $this->redirectToRoute('recipe_detail',['id' => $request->get('id')]);
    }
}

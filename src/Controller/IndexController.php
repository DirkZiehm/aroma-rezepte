<?php

namespace App\Controller;


use App\Repository\BlogArticleRepository;
use App\Repository\RecipeRepository;
use App\Repository\UserRepository;
use App\Repository\UserSetupRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    #[Route('/', name: 'index')]
    public function index(BlogArticleRepository $blogArticleRepository,
                          RecipeRepository $recipeRepository,
                          UserRepository $userRepository,
                          UserSetupRepository $userSetupRepository,
                          Request $request): Response
    {

        $userID =  ($this->getUser() !== null) ? $this->getUser()->getId() : 0;
        $tracker = new TrackerController();
        $tracker->track($request,$this->getDoctrine()->getManager(),$userID);

        $blogArticles = $blogArticleRepository->getOrderedArticles(5);
        $recipes = $recipeRepository->findBy([],['ranking'=>'DESC'],4);
        $creators = $userRepository->findCreators();
        $fullCreators = [];

        foreach ($creators as $creator) {
            $creator->setUserSetup(
                $userSetupRepository->findBy(['userID' => $creator->getId()])[0]
            );

            $creator->setRecipesCount(
                count($recipeRepository->findBy(['user_id' => $creator->getId()]))
            );

            $fullCreators[] = $creator;
        }

        return $this->render('index/index.html.twig', [
            'controller_name' => 'IndexController',
            'blogWidgetItems' => $blogArticles,
            'recipes' => $recipes,
            'creators' => $fullCreators
        ]);
    }
}

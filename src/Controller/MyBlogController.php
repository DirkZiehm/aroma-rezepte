<?php

namespace App\Controller;

use App\Entity\BlogArticle;
use App\Repository\BlogArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MyBlogController extends AbstractController
{
    #[Route('/my/blog', name: 'my_blog')]
    public function index(BlogArticleRepository $blogArticleRepository,Request $request): Response
    {
        $blogArticles = $blogArticleRepository->findBy(['userID' => $this->getUser()->getId()],['id' => 'DESC']);
        return $this->render('my_blog/index.html.twig', [
            'blogArticles' => $blogArticles
        ]);
    }

    #[Route('/my/blog/delete', name: 'blog_delete')]
    public function delete(Request $request, BlogArticleRepository $blogArticleRepository): Response
    {
        $delete = false;
        $blogArticleToDelete = new BlogArticle();

        $blogArticleId = $request->get('id');

        $blogArticles = $blogArticleRepository->findBy(['userID' => $this->getUser()->getId()]);

        foreach ($blogArticles as $blogArticle) {
            if ($blogArticle->getId() === (int)$blogArticleId) {
                $blogArticleToDelete = $blogArticle;
                $delete = true;
                break;
            }
        }

        if (!$delete) {
            return $this->redirectToRoute('index');
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($blogArticleToDelete);

        try{
            $em->flush();

        }catch (\Exception $e){
            return $this->redirectToRoute('index');
        }

        $blogArticles = $blogArticleRepository->findBy(['userID' => $this->getUser()->getId()],['id' => 'DESC']);

        return $this->render('my_blog/index.html.twig', [
            'blogArticles' =>$blogArticles
        ]);
    }

}

<?php

namespace App\Controller;

use App\Entity\Recipe;
use App\Repository\RecipeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MyRecipesController extends AbstractController
{
    #[Route('/my/recipes', name: 'my_recipes')]
    public function index(RecipeRepository $recipeRepository): Response
    {
        $recipes = $recipeRepository->findBy(['user_id' => $this->getUser()->getId()]);

        return $this->render('my_recipes/index.html.twig', [
            'recipes' => $recipes,
        ]);
    }

    #[Route('/my/recipes/delete', name: 'recipe_delete')]
    public function delete(Request $request, RecipeRepository $recipeRepository): Response
    {

        // TODO delete all recipe comments for this recipe

        $id = $request->get('id');
        $delete = false;
        $recipeToDelete = new Recipe();

        $recipes = $recipeRepository->findBy(['user_id' => $this->getUser()->getId()]);

        foreach ($recipes as $recipe){
            if ($recipe->getId() === (int)$id) {
                $recipeToDelete = $recipe;
                $delete = true;
                break;
            }
        }

        if (!$delete) {
            return $this->render('my_recipes/index.html.twig', [
                'recipes' => $recipes,
            ]);
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($recipeToDelete);

        try {
            $em->flush();
        } catch (\Exception $e) {
            $this->addFlash('error', 'Ups! Etwas ist schief gegangen.');
            return $this->render('my_recipes/index.html.twig', [
                'recipes' => $recipeRepository->findBy(['user_id' => $this->getUser()->getId()]),
            ]);
        }


        $this->addFlash('success', 'Rezept erfolgreich gelöscht');
        return $this->render('my_recipes/index.html.twig', [
            'recipes' => $recipeRepository->findBy(['user_id' => $this->getUser()->getId()]),
        ]);
    }
}

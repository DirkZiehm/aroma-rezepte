<?php

namespace App\Controller;

use App\Entity\RecipeComment;
use App\Repository\RecipeCommentRepository;
use App\Repository\RecipeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RatingController extends AbstractController
{
    #[Route('/rating', name: 'rating')]
    public function index(Request $request,
                          RecipeCommentRepository $recipeCommentRepository,
                          RecipeRepository $recipeRepository): Response
    {

        if ($request->getMethod() === 'POST' &&
            $this->getUser()->getId() &&
            $request->get('recipeID') ) {

            $recipeComments = $recipeCommentRepository->findBy([
                'username' => $this->getUser()->getUsername(),
                'recipeID' => $request->get('recipeID')
                ]);

            if (count($recipeComments) != 0) {
                return $this->redirect($request->server->get('HTTP_REFERER'));
            }

            $recipeComment = new RecipeComment();

            $recipeComment->setUsername($this->getUser()->getUsername());
            $recipeComment->setComment($request->get('comment'));
            $recipeComment->setDate(date('d.m.Y'));
            $recipeComment->setRating($request->get('rating'));
            $recipeComment->setRecipeID($request->get('recipeID'));

            $recipe = $recipeRepository->find($request->get('recipeID'));

            $recipe->setRanking(($recipe->getRanking()+$request->get('rating')) / ($recipe->getRatings()+1));
            $recipe->setRatings($recipe->getRatings() + 1);

            $em = $this->getDoctrine()->getManager();
            $em->persist($recipeComment);

            try {
                $em->flush();
                $em->persist($recipe);
                $em->flush();

            } catch (\Exception $e) {
                return $this->redirect($request->server->get('HTTP_REFERER'));
            }

        }else{

            return $this->redirect($request->server->get('HTTP_REFERER'));
        }

        return $this->redirect($request->server->get('HTTP_REFERER'));
    }
}

<?php

namespace App\Controller;

use App\Entity\Recipe;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RecipeController extends AbstractController
{
    #[Route('/recipe', name: 'recipe')]
    public function index(): Response
    {
        $recipeRepo = $this->getDoctrine()->getRepository(Recipe::class);
        $recipes = $recipeRepo->findBy([],['id'=>'DESC']);

        return $this->render('recipe/index.html.twig', [
            'recipes' => $recipes,
        ]);
    }
}

<?php

namespace App\Controller;

use App\Entity\Recipe;
use App\Repository\FavoriteRepository;
use App\Repository\RecipeCommentRepository;
use App\Repository\RecipeRepository;
use App\Repository\UserRepository;
use App\Repository\UserSetupRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RecipeDetailController extends AbstractController
{
    #[Route('/recipe/detail', name: 'recipe_detail')]
    public function index(Request $request,
                          RecipeRepository $recipeRepository ,
                          RecipeCommentRepository $recipeCommentRepository,
                          UserRepository $userRepository,
                          UserSetupRepository $userSetupRepository,
                          FavoriteRepository $favoriteRepository): Response
    {

        $isFavorite = false;
        $userID =  ($this->getUser() !== null) ? $this->getUser()->getId() : 0;
        $tracker = new TrackerController();
        $tracker->track($request,$this->getDoctrine()->getManager(),$userID);

        if (!$recipeID = $request->get('id')) {
            return $this->redirectToRoute('recipe');
        }

        if ($userID) {
            $isFavorite = $favoriteRepository->isFavorites($userID,$recipeID);
        }

        $comments = $recipeCommentRepository->findBy(['recipeID' => $recipeID]);
        $recipe = $recipeRepository->find($recipeID);
        $creator =  $userRepository->find($recipe->getUserId());

        if ($isCreator = $userRepository->isCreator($creator)) {
            $creator->setUserSetup(
                $userSetupRepository->findBy(['userID' => $creator->getId()])[0]
            );
        }

        return $this->render('recipe_detail/index.html.twig', [
            'recipe' => $recipe,
            'comments' => $comments,
            'isCreator' => $isCreator,
            'creator' => $creator,
            'isFavorite' => $isFavorite
        ]);
    }
}

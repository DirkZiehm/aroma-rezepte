<?php

namespace App\Controller;

//use Doctrine\DBAL\Types\TextType;
use App\Entity\User;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

class RegisterController extends AbstractController
{
    #[Route('/register', name: 'register')]
    public function register(Request $request,
                             UserPasswordHasherInterface $userPasswordHasherInterface): Response
    {
        $userID =  ($this->getUser() !== null) ? $this->getUser()->getId() : 0;
        $tracker = new TrackerController();
        $tracker->track($request,$this->getDoctrine()->getManager(),$userID);

        $regForm = $this->createFormBuilder()
            ->add('username', TextType::class,['label' => false,
                'attr' => array(
                'placeholder' => 'Benutzername'
            )])
            ->add('email',EmailType::class,[
                'label' => false,
                'attr' => array(
                    'placeholder' => 'email@domain.de'
                )
            ])
            ->add('password', RepeatedType::class,[
                'type' => PasswordType::class,
                'first_options' => [
                    'label' => false,
                    'attr' => array(
                    'placeholder' => 'Passwort'
                )],
                'second_options' => ['label' => false,
                    'attr' => array(
                    'placeholder' => 'Passwort Wiederholen'
                )]
            ])
        ->add('registration',SubmitType::class,[
            'attr' => array(
            'class' => 'theme-btn-secondary'
        )])
        ->getForm();

        $regForm->handleRequest($request);

        if ($regForm->isSubmitted()) {
            if (!$regForm->isValid()) {
                $this->addFlash('error', 'Ups! Deine Daten scheinen nicht zu stimmen.');
                return $this->redirectToRoute('register');
            }

            $data = $regForm->getData();

            $user = new User();
            $user->setUsername($data['username']);

            $user->setPassword(
              $userPasswordHasherInterface->hashPassword($user,$data['password'])
            );

            $user->setEmail($data['email']);
//            $user->setRoles(['ROLE_ADMIN']);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);

            try {
                $em->flush();
            } catch (\Exception $e) {
                $this->addFlash('error', 'Ups! Etwas ist schief gegangen.');
                return $this->redirectToRoute('register');
            }

            $this->addFlash('success', 'Du hast dich erfolgreich registriert! Bitte melde Dich an.');
            return $this->redirectToRoute('app_login');
        }

        return $this->render('register/index.html.twig', [
            'regForm' => $regForm->createView()
        ]);
    }
}

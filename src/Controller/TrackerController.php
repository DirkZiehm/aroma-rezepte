<?php

namespace App\Controller;

use App\Entity\Tracker;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class TrackerController extends AbstractController
{
    public function track(Request $request,$em,$userID)
    {
        $anonymizeIp = $this->anonymizeIp($request->server->get('REMOTE_ADDR') ?? '');
        $tracker = new Tracker();

        $tracker->setReferer($_SESSION['ref'] ?? '');
        $tracker->setRequesUri($request->server->get('REQUEST_URI') ?? '');
        $tracker->setUserAgent($request->server->get('HTTP_USER_AGENT') ?? '');
        $tracker->setRemoteAddr($this->anonymizeIp($request->server->get('REMOTE_ADDR') ?? ''));
        $tracker->setDate(new \DateTime());
        $tracker->setTime(new \DateTime());
        $tracker->setUserID($userID);
//        var_dump($anonymizeIp);
//        var_dump($this->getBlacklist());
//var_dump(strpos($this->getBlacklist(), $anonymizeIp, 1));

        if (strpos($this->getBlacklist(), $anonymizeIp, 1) === false) {
            $em->persist($tracker);
            $em->flush();
        }

    }

    private function anonymizeIp(string $ip): string
    {
        return substr($ip,0,-4) . 'xxxx';
    }

    private function getBlacklist()
    {
        return file_get_contents('../config/.blacklist', true);
    }


}

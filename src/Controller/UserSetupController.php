<?php

namespace App\Controller;

use App\Entity\UserSetup;
use App\Repository\RecipeRepository;
use App\Repository\UserSetupRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserSetupController extends AbstractController
{
    #[Route('/user/setup', name: 'user_setup')]
    public function index(UserSetupRepository $userSetupRepository, Request $request,RecipeRepository $recipeRepository): Response
    {
        $userSetup = $userSetupRepository->findBy(['userID' => $this->getUser()->getId()])[0];

        $form = $this->getForm($userSetup);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $data = $form->getData();

            $userSetup->setEmail($data->getEmail());
            $userSetup->setUserID($this->getUser()->getId());
            $userSetup->setImage($data->getImage());
            $userSetup->setFacebook($data->getFacebook());
            $userSetup->setLinkedin($data->getLinkedin());
            $userSetup->setLrshop($data->getLrshop());
            $userSetup->setPhone($data->getPhone());
            $userSetup->setTwitter($data->getTwitter());
            $userSetup->setWhatsapp($data->getWhatsapp());
            $userSetup->setInstagram($data->getInstagram());
            $userSetup->setSlogan($data->getSlogan());

            $em = $this->getDoctrine()->getManager();
            $em->persist($userSetup);

            $error = array();

            try {
                $em->flush();
                $form = $this->getForm($userSetup);

            } catch (\Exception $e) {
                $error['err'] = 'fehler';
                return $this->redirectToRoute('register',[$error]);
            }
        }

        return $this->render('user_setup/index.html.twig', [
            'form' => $form->createView(),
            'creatorID' => $this->getUser()->getId()
        ]);
    }

    private function getForm(UserSetup $userSetup)
    {
        return $this->createFormBuilder($userSetup)
            ->add('slogan',TextType::class,[
                'label' => false,
                'required'   => false,
                'attr' => array(
                    'value' => $userSetup->getSlogan(),
                    'placeholder' => 'Ein wunderbarer Slogan passt hier rein',

                )
            ])
            ->add('twitter',TextType::class,[
                'label' => false,
                'required'   => false,
                'attr' => array(
                    'value' => $userSetup->getTwitter(),
                    'placeholder' => 'https://twitter.com/' . $this->getUser()->getUsername(),

                )
            ])
            ->add('whatsapp',TextType::class,[
                'label' => false,
                'required'   => false,
                'attr' => array(
                    'value' => $userSetup->getWhatsapp(),
                    'placeholder' => 'Whatsapp Nummer'
                )
            ])
            ->add('facebook',TextType::class,[
                'label' => false,
                'required'   => false,
                'attr' => array(
                    'value' => $userSetup->getFacebook(),
                    'placeholder' => 'https://de-de.facebook.com/' . $this->getUser()->getUsername()
                )
            ])
            ->add('linkedin',TextType::class,[
                'label' => false,
                'required'   => false,
                'attr' => array(
                    'value' => $userSetup->getlinkedin() ,
                    'placeholder' => 'https://www.linkedin.com/in/' . $this->getUser()->getUsername()
                )
            ])
            ->add('instagram',TextType::class,[
                'label' => false,
                'required'   => false,
                'attr' => array(
                    'value' => $userSetup->getInstagram(),
                    'placeholder' => 'https://www.instagram.com/'
                )
            ])
            ->add('lrshop',TextType::class,[
                'label' => false,
                'required'   => false,
                'attr' => array(
                    'value' => $userSetup->getLrshop(),
                    'placeholder' => 'https://shop.lrworld.com/home/de/de?PHP=xxxxxxx'
                )
            ])
            ->add('phone',TextType::class,[
                'label' => false,
                'required'   => false,
                'attr' => array(
                    'value' => $userSetup->getPhone(),
                    'placeholder' => 'Telefonnummer'
                )
            ])
            ->add('email',TextType::class,[
                'label' => false,
                'required'   => false,
                'attr' => array(
                    'value' => $userSetup->getEmail(),
                    'placeholder' => 'E-Mail Adresse'
                )
            ])
            ->add('image',TextType::class,[
                'label' => false,
                'required'   => false,
                'attr' => array(
                    'value' => $userSetup->getImage(),
                    'placeholder' => 'Bild URL'
                )
            ])
            ->add('SPEICHERN',SubmitType::class,[
                'attr' => array(
                    'class' => 'theme-btn-secondary')])
            ->getForm();
    }
}

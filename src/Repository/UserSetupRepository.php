<?php

namespace App\Repository;

use App\Entity\UserSetup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserSetup|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserSetup|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserSetup[]    findAll()
 * @method UserSetup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserSetupRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserSetup::class);
    }

}
